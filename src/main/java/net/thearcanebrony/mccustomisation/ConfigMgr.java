package net.thearcanebrony.mccustomisation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConfigMgr {
    private static final Gson g = new GsonBuilder().setPrettyPrinting().create();
    private static final String filename = "config/McCustomisation.json";
    public static Config config = new Config();
    public static Config Get() {
        if (!Files.exists(Paths.get(filename))) return config = new Config();
        else return config = g.fromJson(FileU.ReadAllText(filename), Config.class);
    }
    public static void Save() {
        try {
            FileWriter myWriter = new FileWriter(filename);
            myWriter.write(g.toJson(config));
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
