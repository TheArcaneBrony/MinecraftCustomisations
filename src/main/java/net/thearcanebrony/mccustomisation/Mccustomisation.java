package net.thearcanebrony.mccustomisation;

import net.fabricmc.api.ModInitializer;

public class Mccustomisation implements ModInitializer {
    @Override
    public void onInitialize() {
        ConfigMgr.Get();
        ConfigMgr.Save();
        System.out.println("Hi from McCustomisation (config successfully loaded)!");
        System.out.println(String.format("AW%s!~", "O".repeat((int)(Math.random()*15+2))));
    }
}
