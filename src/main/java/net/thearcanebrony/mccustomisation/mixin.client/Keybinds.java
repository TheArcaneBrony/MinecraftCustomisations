package net.thearcanebrony.mccustomisation.mixin.client;

import net.minecraft.client.Keyboard;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Keyboard.class)
public abstract class Keybinds {
    @Shadow protected abstract boolean processDebugKeys(int key);
    @Inject(method = "processF3", at=@At("HEAD"), cancellable = true)
    private void processF3(int key, CallbackInfoReturnable<Boolean> cir){
        processDebugKeys(key);
    }
}
