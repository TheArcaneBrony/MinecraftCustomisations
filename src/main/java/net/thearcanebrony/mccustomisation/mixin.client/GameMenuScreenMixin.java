package net.thearcanebrony.mccustomisation.mixin.client;

import net.minecraft.client.gui.screen.*;
import net.minecraft.client.gui.screen.advancement.AdvancementsScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.screen.option.OptionsScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.realms.gui.screen.RealmsMainScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GameMenuScreen.class)
public class GameMenuScreenMixin extends Screen {
    protected GameMenuScreenMixin(Text title) {
        super(title);
    }

    @Inject(method = "initWidgets", at = @At("HEAD"), cancellable = true)
    private void render(CallbackInfo ci){
        boolean isServer = client.getServer().isRemote();
        addDrawableChild(new ButtonWidget(width / 2 - 102, height / 4 + 24 + -16, 204, 20, new TranslatableText("menu.returnToGame"), (button) -> {
            client.setScreen((Screen)null);
            client.mouse.lockCursor();
        }));
        addDrawableChild(new ButtonWidget(width / 2 - 102, height / 4 + 48 + -16, 98, 20, new TranslatableText("gui.advancements"), (button) -> {
            client.setScreen(new AdvancementsScreen(client.player.networkHandler.getAdvancementHandler()));
        }));
        addDrawableChild(new ButtonWidget(width / 2 + 4, height / 4 + 48 + -16, 98, 20, new TranslatableText("gui.stats"), (button) -> {
            client.setScreen(new StatsScreen(this, client.player.getStatHandler()));
        }));
        ButtonWidget buttonWidget = addDrawableChild(new ButtonWidget(width / 2 + 4, height / 4 + 96 + -16, 98, 20, new TranslatableText("menu.shareToLan"), (button) -> {
            client.setScreen(new OpenToLanScreen(this));
        }));
        boolean smalloptions = buttonWidget.visible = buttonWidget.active = client.isIntegratedServerRunning() && !isServer;

        addDrawableChild(new ButtonWidget(width / 2 - 102, height / 4 + 96 + -16, smalloptions ? 98 : 204, 20, new TranslatableText("menu.options"), (button) -> {
            client.setScreen(new OptionsScreen(this, client.options));
        }));

        Text text = client.isInSingleplayer() ? new TranslatableText("menu.returnToMenu") : new TranslatableText("menu.disconnect");
        addDrawableChild(new ButtonWidget(width / 2 - 102, height / 4 + 120 + -16, 204, 20, text, (button) -> {
            client.world.disconnect();
            if (client.isInSingleplayer()) {
                client.disconnect(new SaveLevelScreen(new TranslatableText("menu.savingLevel")));
            } else {
                client.disconnect();
            }
            TitleScreen titleScreen = new TitleScreen();
            client.setScreen(titleScreen);
        }));
        ci.cancel();
    }
}
