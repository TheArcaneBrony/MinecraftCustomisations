package net.thearcanebrony.mccustomisation.mixin.client;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.BackgroundHelper;
import net.minecraft.client.gui.screen.Overlay;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.SplashOverlay;
import net.minecraft.client.render.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.resource.ResourceReload;
import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(SplashOverlay.class)
public abstract class SplashOverlayMixin {
    @Shadow @Final private MinecraftClient client;
    @Shadow private float progress;
    @Shadow private boolean reloading;
    @Shadow private long reloadCompleteTime;
    @Shadow private long reloadStartTime;
    @Shadow private ResourceReload reload;


    @Shadow
    abstract void renderProgressBar(MatrixStack matrice, int minX, int minY, int maxX, int maxY, float opacity);

    @Shadow
    static int withAlpha(int color, int alpha) {
        return 0;
    }

    private static int BACKGROUND_COLOR = -1;


    @Inject(method = "render", at = @At("HEAD"), cancellable = true)
    private void render(MatrixStack matrices, int mouseX, int mouseY, float delta, CallbackInfo ci){
        if(BACKGROUND_COLOR == -1) BACKGROUND_COLOR = BackgroundHelper.ColorMixer.getArgb(255,0,0,0);

        float h;
        int k;
        float g;
        int width = this.client.getWindow().getScaledWidth();
        int height = this.client.getWindow().getScaledHeight();
        long measuringTime = Util.getMeasuringTimeMs();
        if (this.reloading && this.reloadStartTime == -1L) {
            this.reloadStartTime = measuringTime;
        }
        float f = this.reloadCompleteTime > -1L ? (float)(measuringTime - this.reloadCompleteTime) / 1000.0f : -1.0f;
        float f2 = g = this.reloadStartTime > -1L ? (float)(measuringTime - this.reloadStartTime) / 500.0f : -1.0f;
        if (f >= 1.0f) {
            if (this.client.currentScreen != null) {
                this.client.currentScreen.render(matrices, 0, 0, delta);
            }
            k = MathHelper.ceil((1.0f - MathHelper.clamp(f - 1.0f, 0.0f, 1.0f)) * 255.0f);
            SplashOverlay.fill(matrices, 0, 0, width, height, withAlpha(BACKGROUND_COLOR, k));
            h = 1.0f - MathHelper.clamp(f - 1.0f, 0.0f, 1.0f);
        } else if (this.reloading) {
            if (this.client.currentScreen != null && g < 1.0f) {
                this.client.currentScreen.render(matrices, mouseX, mouseY, delta);
            }
            k = MathHelper.ceil(MathHelper.clamp((double)g, 0.15, 1.0) * 255.0);
            SplashOverlay.fill(matrices, 0, 0, width, height, withAlpha(BACKGROUND_COLOR, k));
            h = MathHelper.clamp(g, 0.0f, 1.0f);
        } else {
            k = BACKGROUND_COLOR;
            float m = (float)(k >> 16 & 0xFF) / 255.0f;
            float n = (float)(k >> 8 & 0xFF) / 255.0f;
            float o = (float)(k & 0xFF) / 255.0f;
            GlStateManager._clearColor(m, n, o, 1.0f);
            GlStateManager._clear(16384, MinecraftClient.IS_SYSTEM_MAC);
            h = 1.0f;
        }
        k = (int)((double)this.client.getWindow().getScaledWidth() * 0.5);
        int m = (int)((double)this.client.getWindow().getScaledHeight() * 0.5);
        double n = Math.min((double)this.client.getWindow().getScaledWidth() * 0.75, (double)this.client.getWindow().getScaledHeight()) * 0.25;
        int p = (int)(n * 0.5);
        double d = n * 4.0;
        int q = (int)(d * 0.5);
//        RenderSystem.setShaderTexture(0, LOGO);
//        RenderSystem.enableBlend();
//        RenderSystem.blendEquation(32774);
//        RenderSystem.blendFunc(770, 1);
//        RenderSystem.setShader(GameRenderer::getPositionTexShader);
//        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, h);
//        SplashOverlay.drawTexture(matrices, k - q, m - p, q, (int)n, -0.0625f, 0.0f, 120, 60, 120, 120);
//        SplashOverlay.drawTexture(matrices, k, m - p, q, (int)n, 0.0625f, 60.0f, 120, 60, 120, 120);
//        RenderSystem.defaultBlendFunc();
//        RenderSystem.disableBlend();
        int r = (int)((double)this.client.getWindow().getScaledHeight() * 0.8325);
        float s = reload.getProgress();
        progress = MathHelper.clamp(this.progress * 0.95f + s * 0.050000012f, 0.0f, 1.0f);
        if (f < 1.0f) {
            renderProgressBar(matrices, width / 2 - q, r - 5, width / 2 + q, r + 5, 1.0f - MathHelper.clamp(f, 0.0f, 1.0f));
        }
        if (f >= 2.0f) {
            this.client.setOverlay(null);
        }
//        if (this.reloadCompleteTime == -1L && this.reload.isComplete() && (!this.reloading || g >= 2.0f)) {
//            try {
//                this.reload.throwException();
//                this.exceptionHandler.accept(Optional.empty());
//            }
//            catch (Throwable throwable) {
//                this.exceptionHandler.accept(Optional.of(throwable));
//            }
//            this.reloadCompleteTime = Util.getMeasuringTimeMs();
//            if (this.client.currentScreen != null) {
//                this.client.currentScreen.init(this.client, this.client.getWindow().getScaledWidth(), this.client.getWindow().getScaledHeight());
//            }
//        }
        ci.cancel();
    }
}
