package net.thearcanebrony.mccustomisation.mixin.client;

import com.google.common.base.Strings;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.hud.DebugHud;
import net.minecraft.client.gui.screen.SplashOverlay;
import net.minecraft.client.render.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.MetricsData;
import net.minecraft.util.math.AffineTransformation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Matrix4f;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.w3c.dom.Text;

import java.util.List;
import java.util.Objects;

@Mixin(DebugHud.class)
public abstract class DebugHudMixin extends DrawableHelper {
    @Shadow @Final private MinecraftClient client;
    @Shadow @Final private TextRenderer textRenderer;

    @Redirect(method="renderLeftText", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/hud/DebugHud;fill(Lnet/minecraft/client/util/math/MatrixStack;IIIII)V"))
    private void fillLeft(MatrixStack matrices, int x1, int y1, int x2, int y2, int color){ }
    @Redirect(method="renderRightText", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/hud/DebugHud;fill(Lnet/minecraft/client/util/math/MatrixStack;IIIII)V"))
    private void fillRight(MatrixStack matrices, int x1, int y1, int x2, int y2, int color){ }
/*    @Inject(method="drawMetricsData", at = @At("HEAD"), cancellable = true)
    private void drawMetricsData(MatrixStack matrices, MetricsData metricsData, int x, int width, boolean showFps, CallbackInfo ci){
        RenderSystem.disableDepthTest();
        int i = metricsData.getStartIndex();
        int j = metricsData.getCurrentIndex();
        long[] ls = metricsData.getSamples();
        int l = x;
        int m = Math.max(0, ls.length - width);
        int n = ls.length - m;
        int k = metricsData.wrapIndex(i + m);
        long o = 0L;
        int p = Integer.MAX_VALUE;
        int q = Integer.MIN_VALUE;

        int t;
        for (t = 0; t < n; ++t) {
            int s = (int) (ls[metricsData.wrapIndex(k + t)] / 1000000L);
            p = Math.min(p, s);
            q = Math.max(q, s);
            o += (long) s;
        }

        t = client.getWindow().getScaledHeight();
        fill(matrices, x, t - 60, x + n, t, -1873784752);
        RenderSystem.setShader(GameRenderer::getPositionColorShader);
        BufferBuilder bufferBuilder = Tessellator.getInstance().getBuffer();
        RenderSystem.enableBlend();
        RenderSystem.disableTexture();
        RenderSystem.defaultBlendFunc();
        bufferBuilder.begin(VertexFormat.DrawMode.LINES, VertexFormats.POSITION_COLOR);

        for (Matrix4f matrix4f = AffineTransformation.identity().getMatrix(); k != j; k = metricsData
                .wrapIndex(k+1)) {
            int u = metricsData.method_15248(ls[k], showFps ? 30 : 60, showFps ? 60 : 20);
            int v = showFps ? 100 : 60;
            int w = getMetricsLineColor(MathHelper.clamp((int) u, (int) 0, (int) v), 0, v / 2, v);
            int y = w >> 24 & 255;
            int z = w >> 16 & 255;
            int aa = w >> 8 & 255;
            int ab = w & 255;
//            bufferBuilder.vertex(matrix4f, (float) (l + 1), (float) t, 0.0F).color(z, aa, ab, y).next();
//            bufferBuilder.vertex(matrix4f, (float) (l + 1), (float) (t - u + 1), 0.0F).color(z, aa, ab, y).next();
            bufferBuilder.vertex(matrix4f, (float) l/2, (float) (t - u + 1), 0.0F).color(z, aa, ab, y).next();
            bufferBuilder.vertex(matrix4f, (float) l/2, (float) t, 0.0F).color(z, aa, ab, y).next();
            ++l;
        }

        bufferBuilder.end();
        BufferRenderer.draw(bufferBuilder);
        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
        if (showFps) {
            fill(matrices, x + 1, t - 30 + 1, x + 14, t - 30 + 10, -1873784752);
            textRenderer.draw(matrices, "60 FPS", (float) (x + 2), (float) (t - 30 + 2), 14737632);
            drawHorizontalLine(matrices, x, x + n - 1, t - 30, -1);
            fill(matrices, x + 1, t - 60 + 1, x + 14, t - 60 + 10, -1873784752);
            textRenderer.draw(matrices, "30 FPS", (float) (x + 2), (float) (t - 60 + 2), 14737632);
            drawHorizontalLine(matrices, x, x + n - 1, t - 60, -1);
        } else {
            fill(matrices, x + 1, t - 60 + 1, x + 14, t - 60 + 10, -1873784752);
            textRenderer.draw(matrices, "20 TPS", (float) (x + 2), (float) (t - 60 + 2), 14737632);
            drawHorizontalLine(matrices, x, x + n - 1, t - 60, -1);
        }

        drawHorizontalLine(matrices, x, x + n - 1, t - 1, -1);
        drawVerticalLine(matrices, x, t - 60, t, -1);
        drawVerticalLine(matrices, x + n - 1, t - 60, t, -1);
        if (showFps && client.options.maxFps > 0 && client.options.maxFps <= 250) {
            drawHorizontalLine(matrices, x, x + n - 1,
                    t - 1 - (int) (1800.0D / (double) client.options.maxFps), -16711681);
        }

        String string = p + " ms min";
        String string2 = o / (long) n + " ms avg";
        String string3 = q + " ms max";
        TextRenderer var10000 = textRenderer;
        float var10003 = (float) (x + 2);
        int var10004 = t - 60;
        Objects.requireNonNull(textRenderer);
        var10000.drawWithShadow(matrices, string, var10003, (float) (var10004 - 9), 14737632);
        var10000 = textRenderer;
        var10003 = (float) (x + n / 2 - textRenderer.getWidth(string2) / 2);
        var10004 = t - 60;
        Objects.requireNonNull(textRenderer);
        var10000.drawWithShadow(matrices, string2, var10003, (float) (var10004 - 9), 14737632);
        var10000 = textRenderer;
        var10003 = (float) (x + n - textRenderer.getWidth(string3));
        var10004 = t - 60;
        Objects.requireNonNull(textRenderer);
        var10000.drawWithShadow(matrices, string3, var10003, (float) (var10004 - 9), 14737632);
        RenderSystem.enableDepthTest();
        ci.cancel();
    }

    private int getMetricsLineColor(int value, int greenValue, int yellowValue, int redValue) {
        return value < yellowValue ? this.interpolateColor(-16711936, -256, (float) value / (float) yellowValue)
                : this.interpolateColor(-256, -65536, (float) (value - yellowValue) / (float) (redValue - yellowValue));
    }

    private int interpolateColor(int color1, int color2, float dt) {
        int i = color1 >> 24 & 255;
        int j = color1 >> 16 & 255;
        int k = color1 >> 8 & 255;
        int l = color1 & 255;
        int m = color2 >> 24 & 255;
        int n = color2 >> 16 & 255;
        int o = color2 >> 8 & 255;
        int p = color2 & 255;
        int q = MathHelper.clamp((int) ((int) MathHelper.lerp(dt, (float) i, (float) m)), (int) 0, (int) 255);
        int r = MathHelper.clamp((int) ((int) MathHelper.lerp(dt, (float) j, (float) n)), (int) 0, (int) 255);
        int s = MathHelper.clamp((int) ((int) MathHelper.lerp(dt, (float) k, (float) o)), (int) 0, (int) 255);
        int t = MathHelper.clamp((int) ((int) MathHelper.lerp(dt, (float) l, (float) p)), (int) 0, (int) 255);
        return q << 24 | r << 16 | s << 8 | t;
    }
*/
}
