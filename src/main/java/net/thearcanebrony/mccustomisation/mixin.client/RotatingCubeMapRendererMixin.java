package net.thearcanebrony.mccustomisation.mixin.client;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.render.*;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(net.minecraft.client.gui.RotatingCubeMapRenderer.class)
public class RotatingCubeMapRendererMixin {
    float width = 10000;
    float height = 10000;
    float textureSize = 32.0F;

    @Inject(method = "render", at = @At("HEAD"), cancellable = true)
    private void renderDirtBackground(float delta, float alpha, CallbackInfo ci){
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferBuilder = tessellator.getBuffer();
        RenderSystem.setShader(GameRenderer::getPositionTexColorShader);
        RenderSystem.setShaderTexture(0, Screen.OPTIONS_BACKGROUND_TEXTURE);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_TEXTURE_COLOR);
        bufferBuilder.vertex(0.0D, height, 0.0D).texture(0.0F, height / textureSize).color(64, 64, 64, 255).next();
        bufferBuilder.vertex(width, height, 0.0D).texture(width / textureSize, height / textureSize).color(64, 64, 64, 255).next();
        bufferBuilder.vertex(width, 0.0D, 0.0D).texture(width / textureSize, 0F).color(64, 64, 64, 255).next();
        bufferBuilder.vertex(0.0D, 0.0D, 0.0D).texture(0.0F, 0F).color(64, 64, 64, 255).next();
        tessellator.draw();
        ci.cancel();
    }
}
