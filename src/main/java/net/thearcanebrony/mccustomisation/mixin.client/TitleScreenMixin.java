package net.thearcanebrony.mccustomisation.mixin.client;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.SharedConstants;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerWarningScreen;
import net.minecraft.client.gui.screen.option.AccessibilityOptionsScreen;
import net.minecraft.client.gui.screen.option.LanguageOptionsScreen;
import net.minecraft.client.gui.screen.option.OptionsScreen;
import net.minecraft.client.gui.screen.pack.PackScreen;
import net.minecraft.client.gui.screen.world.SelectWorldScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.ClickableWidget;
import net.minecraft.client.gui.widget.TexturedButtonWidget;
import net.minecraft.client.realms.gui.screen.RealmsNotificationsScreen;
import net.minecraft.client.render.*;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.resource.ResourcePackManager;
import net.minecraft.resource.ResourcePackProfile;
import net.minecraft.server.MinecraftServer;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3f;
import net.minecraft.util.registry.DynamicRegistryManager;
import net.minecraft.world.gen.GeneratorOptions;
import net.thearcanebrony.mccustomisation.ConfigMgr;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.*;
import java.util.function.Consumer;

@Mixin(TitleScreen.class)
public abstract class TitleScreenMixin extends Screen {
    @Shadow protected abstract void initWidgetsDemo(int y, int spacingY);
    @Shadow protected abstract void initWidgetsNormal(int y, int spacingY);
    @Shadow private String splashText;
    @Shadow private int copyrightTextX, copyrightTextWidth;
    @Shadow private boolean isMinceraft;
    @Shadow private static Identifier MINECRAFT_TITLE_TEXTURE;

    float textureSize = 32.0F;

    protected TitleScreenMixin(Text title) {
        super(title);
    }

    @Inject(method = "areRealmsNotificationsEnabled", at = @At("HEAD"), cancellable = true)
    private void noReamls(CallbackInfoReturnable<Boolean> cir){
        cir.setReturnValue(false);
    }
    @Inject(method="initWidgetsNormal", at = @At("HEAD"), cancellable = true)
    private void addButtons(int y, int spacingY, CallbackInfo ci) {
        this.addDrawableChild(new ButtonWidget(this.width / 2 - 100, y, 200, 20, new TranslatableText("menu.singleplayer"), (button) -> {
            this.client.setScreen(new SelectWorldScreen(this));
        }));
        this.addDrawableChild(new ButtonWidget(this.width / 2 - 100, y + spacingY * 1, 200, 20, new TranslatableText("menu.multiplayer"), (button) -> {
            this.client.setScreen(new MultiplayerWarningScreen(this));
        }));
        this.addDrawableChild(new ButtonWidget(this.width / 2 - 100, y + spacingY * 2, 200, 20, new TranslatableText("options.resourcepack"), (button) -> {
            this.client.setScreen(new PackScreen(this, this.client.getResourcePackManager(), this::refreshResourcePacks, this.client.getResourcePackDir(), new TranslatableText("resourcePack.title")));
        }));
        ci.cancel();
    }
    @Inject(method="init", at = @At("HEAD"), cancellable = true)
    protected void init(CallbackInfo ci){
        if (splashText == null) {
            splashText = this.client.getSplashTextLoader().get();
        }

        int j = this.height / 4 + 48;
        if (this.client.isDemo()) {
            initWidgetsDemo(j, 24);
        } else {
            initWidgetsNormal(j, 24);
        }

        this.addDrawableChild(new ButtonWidget(this.width / 2 - 100, j + 72 + 12, 98, 20, new TranslatableText("menu.options"), (button) -> {
            this.client.setScreen(new OptionsScreen(this, this.client.options));
        }));
        this.addDrawableChild(new ButtonWidget(this.width / 2 + 2, j + 72 + 12, 98, 20, new TranslatableText("menu.quit"), (button) -> {
            this.client.scheduleStop();
        }));
        this.client.setConnectedToRealms(false);

        ci.cancel();
    }
    @Inject(method="render", at = @At("HEAD"), cancellable = true)
    protected void render(MatrixStack matrices, int mouseX, int mouseY, float delta, CallbackInfo ci){
        copyrightTextWidth = this.textRenderer.getWidth("Copyright Mojang AB. Do not distribute!");
        copyrightTextX = this.width - copyrightTextWidth - 2;
        int j = this.width / 2 - 137;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferBuilder = tessellator.getBuffer();
        RenderSystem.setShader(GameRenderer::getPositionTexColorShader);
        RenderSystem.setShaderTexture(0, Screen.OPTIONS_BACKGROUND_TEXTURE);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_TEXTURE_COLOR);
        bufferBuilder.vertex(0.0D, height, 0.0D).texture(0.0F, height / textureSize).color(64, 64, 64, 255).next();
        bufferBuilder.vertex(width, height, 0.0D).texture(width / textureSize, height / textureSize).color(64, 64, 64, 255).next();
        bufferBuilder.vertex(width, 0.0D, 0.0D).texture(width / textureSize, 0F).color(64, 64, 64, 255).next();
        bufferBuilder.vertex(0.0D, 0.0D, 0.0D).texture(0.0F, 0F).color(64, 64, 64, 255).next();
        tessellator.draw();
        float g = 1.0F;
        int l = MathHelper.ceil(g * 255.0F) << 24;
        if ((l & -67108864) != 0) {
            RenderSystem.setShader(GameRenderer::getPositionTexShader);
            RenderSystem.setShaderTexture(0, MINECRAFT_TITLE_TEXTURE);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, g);
            /*if (isMinceraft) {
                this.drawWithOutline(j, 30, (x, y) -> {
                    this.drawTexture(matrices, x + 0, y, 0, 0, 99, 44);
                    this.drawTexture(matrices, x + 99, y, 129, 0, 27, 44);
                    this.drawTexture(matrices, x + 99 + 26, y, 126, 0, 3, 44);
                    this.drawTexture(matrices, x + 99 + 26 + 3, y, 99, 0, 26, 44);
                    this.drawTexture(matrices, x + 155, y, 0, 45, 155, 44);
                });
            } else {
                this.drawWithOutline(j, 40, (x, y) -> {
                    this.drawTexture(matrices, x + 0, y, 0, 0, 155, 44);
                    this.drawTexture(matrices, x + 155, y, 0, 45, 155, 44);
                });
            }*/
            renderLogo(j, matrices);
            if (this.splashText != null) {
                matrices.push();
                matrices.translate((double)(this.width / 2 + 90), 70.0D, 0.0D);
                matrices.multiply(Vec3f.POSITIVE_Z.getDegreesQuaternion(-20.0F));
                float h = 1.8F - MathHelper.abs(MathHelper.sin((float)(Util.getMeasuringTimeMs() % 1000L) / 1000.0F * 6.2831855F) * 0.1F);
                h = h * 100.0F / (float)(this.textRenderer.getWidth(this.splashText) + 32);
                matrices.scale(h, h, h);
                drawCenteredText(matrices, this.textRenderer, this.splashText, 0, -8, 16776960 | l);
                matrices.pop();
            }

            String string = "Minecraft " + SharedConstants.getGameVersion().getName() + (client.isDemo() ? " Demo" : "/" + client.getVersionType()) + (client.isModded() ? I18n.translate("menu.modded", new Object[0]) : "");

            drawStringWithShadow(matrices, this.textRenderer, string, 2, 2, 16777215 | l);
            drawStringWithShadow(matrices, this.textRenderer, "Copyright Mojang AB. Do not distribute!", this.copyrightTextX, this.height - 10, 16777215 | l);
            if (mouseX > this.copyrightTextX && mouseX < this.copyrightTextX + copyrightTextWidth && mouseY > this.height - 10 && mouseY < this.height) {
                fill(matrices, this.copyrightTextX, this.height - 1, this.copyrightTextX + copyrightTextWidth, this.height, 16777215 | l);
            }

            client.getWindow().setTitle(String.format("Frame %s | pxsz %s | %s", frame, px_sz, client.fpsDebugString));
            super.render(matrices, mouseX, mouseY, delta);

        }
        ci.cancel();
    }

    private int frame = 0;
    private int px_sz = 2;
    private Random rnd = new Random();
    private HashMap<String, Double> rands = new HashMap<String, Double>();
    private void renderLogo(int j, MatrixStack matrices){
        if (isMinceraft) {
            this.drawWithOutline(j, 30, (x, y) -> {
                this.drawTexture(matrices, x + 0, y, 0, 0, 99, 44);
                this.drawTexture(matrices, x + 99, y, 129, 0, 27, 44);
                this.drawTexture(matrices, x + 99 + 26, y, 126, 0, 3, 44);
                this.drawTexture(matrices, x + 99 + 26 + 3, y, 99, 0, 26, 44);
                this.drawTexture(matrices, x + 155, y, 0, 45, 155, 44);
            });
        } else {
            this.drawWithOutline(j, 40, (x, y) -> {
                if (frame < 300 && ConfigMgr.config.logoAnimationEnabled) {
                    double f = 5d/(double)frame;
                    px_sz = Math.min((frame/5)+1, 5);
                    for (int i = 0; i < 155 + 155; i += px_sz) {
                        int tex_x = i % 155;
                        int x_dist = (int) ((i - 155) * f);
                        for (int k = 0; k < 44; k += px_sz) {
                            int tex_y = (i < 155 ? 0 : 45) + k;
                            int y_dist = (int) ((k - 22) * f);
                            if(!rands.containsKey(i + "." + k)) rands.put(i + "." + k, (double) rnd.nextFloat());
                            y_dist *= rands.get(i+"."+k);

                            this.drawTexture(matrices, (int) (x + i + x_dist * rands.get(i+"."+k)), y + k + y_dist, tex_x, tex_y, px_sz, px_sz);
                        }
                    }
                }
                else {
                    this.drawTexture(matrices, x, y, 0, 0, 155, 44);
                    this.drawTexture(matrices, x + 155, y, 0, 45, 155, 44);
                }
            });
        }
        frame++;
    }



    private void refreshResourcePacks(ResourcePackManager resourcePackManager) {
        List<String> list = ImmutableList.copyOf((Collection)client.options.resourcePacks);
        client.options.resourcePacks.clear();
        client.options.incompatibleResourcePacks.clear();

        for (ResourcePackProfile resourcePackProfile : resourcePackManager.getEnabledProfiles()) {
            if (!resourcePackProfile.isPinned()) {
                client.options.resourcePacks.add(resourcePackProfile.getName());
                if (!resourcePackProfile.getCompatibility().isCompatible()) {
                    client.options.incompatibleResourcePacks.add(resourcePackProfile.getName());
                }
            }
        }

        client.options.write();
        List<String> list2 = ImmutableList.copyOf((Collection)client.options.resourcePacks);
        if (!list2.equals(list)) {
            this.client.reloadResources();
        }
    }
}
