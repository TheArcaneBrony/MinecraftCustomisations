package net.thearcanebrony.mccustomisation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileU {
    public static String ReadAllText(String path)  {
        return Arrays.stream(ReadLines(path)).collect(Collectors.joining(System.lineSeparator()));
    }
    public static String[] ReadLines(String path) {
        try{
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line;
            List<String> lines = new ArrayList<>();
            while((line = reader.readLine()) != null){
                lines.add(line);
            }
            return lines.toArray(new String[0]);
        }
        catch (IOException e){
            return new String[] {"An error occurred reading the file!"};
        }
    }
}
